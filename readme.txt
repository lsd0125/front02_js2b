
準備工作：

1. 環境必須裝好 node.js

2. 必須安裝 MySQL (用戶: root, 密碼: root)
    若欲變更帳密可修改 app.js 裡， MySQL 連線的程式碼

3. 將 /front01_js2b.sql 匯入 MySQL

4. 可安裝 nodemon (選擇性安裝)
    $ sudo npm i -g nodemon

5. 在專案的工作目錄，安裝專案所需的套件
    $ npm install

6. 啟動 app (若有安裝 nodemon 用第一種)
    $ nodemon app.js
    $ node app.js

7. 拜訪網頁
    http://localhost:3000/
    http://localhost:3000/data
