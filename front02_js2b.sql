-- Database: `front02_js2b`
--
CREATE DATABASE IF NOT EXISTS `front02_js2b` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `front02_js2b`;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `sid` int(11) NOT NULL,
  `sales_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`sid`, `sales_id`, `name`, `birthday`, `create_at`) VALUES
(1, 'A001', '林比爾', '1989-03-03', '2018-11-19 15:20:52'),
(2, 'A003', '陳彼德', '1987-02-08', '2018-11-19 15:21:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`sid`),
  ADD UNIQUE KEY `sales_id` (`sales_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;