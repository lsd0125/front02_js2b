var express = require('express');

var app = express();
var exphbs = require('express-handlebars');

// 設定使用的樣板引擎, 使用 main 主檔名的 layout
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
// 設定使用的樣板引擎
app.set('view engine', 'handlebars');


// 把 public 資料夾設定成靜態的內容路徑
app.use(express.static('public'));
app.use(express.static('node_modules/bootstrap/dist'));


// routes 路由
app.get('/', (req, res)=>{
    //res.send('Hello world');
    res.redirect('/data');
});

app.get('/a', (req, res)=>{
    res.render('a', {
        who: 'Shinder'
    });
});

app.get('/sales', (req, res)=>{
    // 載入 json 資料
    const sales = require('./data/sales.json');

    res.render('sales', {
        sales: sales
    });
});

const url = require('url');

// 接收 get 參數 (query string)
app.get('/param', (req, res)=>{
    const urlParts = url.parse(req.url, true);
    console.log(urlParts);
    res.send( JSON.stringify(urlParts));
});

const bodyParser = require('body-parser');
// 取得解析資料的 middleware
const urlencodedParser = bodyParser.urlencoded({extended:false});
// 接收 post 參數 (表單資料)
app.post('/param', urlencodedParser, (req, res)=>{
    console.log(req.body);
    res.send( req.body.email + ':::' + req.body.password);
});

app.put('/param2', urlencodedParser, (req, res)=>{
    console.log(req.body);
    res.send('1111');
});

// 使用 regular expression 設定路由
app.get(/^\/hi\/?/, function(request, response){
    const url_str = request.url.slice(4); // 排除前面的 /hi/
    const url_ar = url_str.split('/');
    const url_json = JSON.stringify(url_ar);

    response.send(`<div>
        ${url_str} <br>
        ${url_json}
    </div>`);
});


const multer = require('multer');
const upload = multer({dest: 'uploads/'}); // 需先建立暫存檔案的資料夾
const fs = require('fs'); // 核心套件
app.post('/upload', upload.single('image_file'), (req, res)=>{
    console.log(req.file);
    /* {
        fieldname: 'image_file',
        originalname: 'test.png',
        encoding: '7bit',
        mimetype: 'image/png',
        destination: 'uploads/',
        filename: 'c49d84d29bc209762dc53288c6d863d3',
        path: 'uploads/c49d84d29bc209762dc53288c6d863d3',
        size: 36933
    } */

    if(req.file && req.file.originalname){
        let filename = req.file.originalname.toLowerCase(); // 原檔名轉成小寫

        // 判斷是否為圖檔
        if(/\.(jpg|jpeg|png)$/.test(filename)){
            // 將檔案搬至公開的資料夾
            fs.createReadStream(req.file.path)
                .pipe( fs.createWriteStream('./public/img/' + req.file.originalname) );
        }
    }
    console.log(req.body);
    res.send(JSON.stringify(req.body));
});

const mysql = require('mysql');
const db = mysql.createConnection({
    host: 'localhost',
    // port: 8889, // 若不是預設埠號 3306, 需要指定
    user: 'root',
    password: 'root',
    database: 'front02_js2b'
});
db.connect();
const moment = require('moment');
app.get('/data', (req, res)=> {
    const sql = "SELECT * FROM `sales` ORDER BY sid DESC";
    db.query(sql, (error, results, fields)=>{
        if(error){
            throw error;
        }
        //console.log(results);

        results.forEach(el => {
            el.birthday = moment(el.birthday).format('YYYY/MM/DD');
            el.create_at = moment(el.create_at).format('YYYY-MM-DD HH:mm:ss');
        });

        //res.json(results); // 輸出 json 格式的資料

        res.render('sales2', {
            sales: results
        });
    });
});

// 新增資料的表單
app.get('/data/add', (req, res)=> {
    res.render('sales2_add');
});
app.post('/data/add', urlencodedParser, (req, res)=> {
    let sql = `INSERT INTO sales(
    sales_id, name, birthday, create_at
    ) VALUES (?, ?, ?, NOW()
    );`;

    db.query(sql, [req.body.sales_id, req.body.name, req.body.birthday], (error, results, fields)=>{
        if(error){
            throw error;
        }
        res.json(results);
    });
});

// 刪除
app.get('/data/delete/:sid', (req, res)=> {
    let sql = `DELETE FROM sales WHERE sid=?`;

    db.query(sql, [req.params.sid], (error, results, fields)=>{
        if(error){
            throw error;
        }
        res.json(results);
    });
});


// 編輯資料
app.get('/data/edit/:sid', (req, res)=> {
    let sql = `SELECT * FROM sales WHERE sid=?`;

    db.query(sql, [req.params.sid], (error, results, fields)=>{
        if(error){
            throw error;
        }
        //console.log(results);

        // 變更生日的格式
        results[0].birthday = moment(results[0].birthday).format('YYYY-MM-DD');

        res.render('sales2_edit', {
            item: results[0]
        });
    });

});
app.post('/data/edit/:sid', urlencodedParser, (req, res)=> {
    let sql = "UPDATE `sales` SET `sales_id`=?,`name`=?,`birthday`=? WHERE sid=?";

    db.query(sql, [
        req.body.sales_id,
        req.body.name,
        req.body.birthday,
        req.params.sid
    ], (error, results, fields)=>{
        if(error){
            throw error;
        }
        res.json(results);
    });
});

// 使用瀏覽器查看 http://localhost:3000/a.html
app.get('/a.html', function(request, response){
    response.send('Ha Ha!!');
});

// 啟動 server, 偵聽 3000 埠
app.listen(3000, function(){
    console.log('starting: 3000!');
});



